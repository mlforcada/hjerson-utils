#  run-hjerson.py
#  
#  Copyright 2020 Mikel L. Forcada
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#  This program reads two plain-text files
#  (1) the output of a machine translation system of a text, and
#  (2) a reference translation of the same text
#  Then it tokenizes and lemmatizes each of the files
#  and runs them through Hjerson to produce a report.

import argparse  # parse args 
import functools  # I might need it to redefine functions
# from collections import Counter # para contar las palabras de mayor frecuencia
# I don't think I need this last one

# tokenize and truecase (do I need true-casing?)
from sacremoses import MosesTokenizer, MosesTruecaser

# I might have to call something as a subprocess
import subprocess

# To delete temporary files 
import os

# To lemmatize in Spanish (not optimal) 
import spacy


# Tokenizer
from sacremoses import MosesTokenizer
mt = MosesTokenizer()

# Where's hjerson
hjersonpy="../hjerson/hjerson.py"

# Ensuring that all open() calls are in utf-8
# by redefining open(). This is necessary; instead, the 
# program will use the native encoding of the platform
# where it is executed
open = functools.partial(open, encoding='utf8') 

# Read the arguments 
parser = argparse.ArgumentParser()

# Args:
# Hypothesis file, Reference file, and language.

parser.add_argument("prefix", help="Filename prefix") 
parser.add_argument("lang", help="Language")      # Language
parser.add_argument("spacymodel",help="Spacy model")    # Spacy language model
args=parser.parse_args()


hypfilename=args.prefix+".hyp"
reffilename=args.prefix+".ref"
# The two files are opened
hypfile=open(hypfilename)	
reffile=open(reffilename)

# Load language model
nlp=spacy.load(args.spacymodel)




# I will probably need something to ensure that sentences are correctly processed
# it currently assumes one sentence per line

# Also, the input to hjerson has to be tokenized



for filename in [hypfilename,reffilename] :
	with open(filename+".tok","w") as outfile :
		[outfile.write(mt.tokenize(line,return_str=True)+"\n") for line in open(filename)]
	with open(filename+".tok"+".base", "w") as outfile :
		[outfile.write(" ".join([w.lemma_ for w in nlp(line.lower())])) for line in open(filename+".tok")]
	# Doesn't seem to work well : produces an extra SPACE token and crashes hjerson
#	with open(filename+".tok"+".cat", "w") as outfile :
#		[outfile.write(" ".join([w.pos_ for w in nlp(line.lower())])) for line in open(filename+".tok")]
	
    # lowercasing used as poor man's truecasing
    
    
hyp=hypfilename+".tok"
ref=reffilename+".tok"
basehyp=hypfilename+".tok.base"
baseref=reffilename+".tok.base"
# addhyp=hypfilename+".tok.cat"
# addref=reffilename+".tok.cat"

# now move on to running hjerson
# and processing its output


# could have used ".format()"
command= "python "+hjersonpy+  \
                 " --ref "+ref+  \
                 " --hyp "+hyp+  \
                 " --baseref "+baseref+ \
                 " --basehyp "+basehyp +\
                 " --sent "+ args.prefix+".errors"\
                 " --html "+ args.prefix+".html"


#                  " --addhyp "+addhyp+\
#                 " --addref "+addref+\


# print(command)
subprocess.call(command,shell=True)


exit()
