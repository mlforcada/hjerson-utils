# hjerson-utils

A Python script for students of Translation Technologies at the Universitat d'Alacant to apply hjerson (Popović 2011) to their Spanish texts on Windows.
Currently a number of options are hardwired; they may have to be changed for oher installations.

## Installing on Windows

### Installing Python and git
First we need to install Python to run the script and git to be able to clone hjerson and this repository

*  Install python (for instance from the Start menu of Windows)
*  Install git from [here](https://git-scm.com/download/win) (not the "thumbdrive version")

### Installing needed software and resources

*  Start a Windows shell (for instance by launching "cmd")
*  Then type the following, each line followed by a newline/enter; first install spacy (language model, takes long)
```
pip install spacy
```
* Then download the Spanish model
```
python -m spacy download es_core_news_sm

```
This is a large file, be patient. The last line should be `You can now load the model via spacy.load('es_core_news_sm')` 

Then install sacremoses (We'll use to tokenize the input)
```
pip install sacremoses
```

* Then clone the hjerson repository
```
git clone https://github.com/cidermole/hjerson
```
* Then clone this repository
```
git clone https://gitlab.com/mlforcada/hjerson-utils
```

### Testing if everything works
Whenever we want to run things, we'll move to the `hjerson-utils` directory after starting the Windows shell.
```
cd hjerson-utils
```
Let's see what's there. There are two small example files, `a.hyp` and `a.ref`, containing a MT hypothesis and a reference translation. 
Each sentence is in a line. Both files have to align
```
dir
```
If we ask Python run the `run-hjerson.py` script without specifying any file but asking for help
```
python run-hjerson.py --help
``` 
it should return a message explaining how it should be used
```
usage: run-hjerson.py [-h] prefix lang spacymodel

positional arguments:
  prefix      Filename prefix
  lang        Language
  spacymodel  Spacy model

optional arguments:
  -h, --help  show this help message and exit
```
Instead of filenames, a prefix is given; the suffixes `.hyp` and `.ref` will complete the input filenames. Let's try it on the files provided. 
```
python run-hjerson.py a es es_core_news_sm
```

If all goes well, the program will produce a report
```
Wer:  	10	31.25
Rper: 	7	21.88
Hper: 	4	13.79

SUMerr: 	7	23.49

rINFer: 	2	6.25
hINFer: 	2	6.90
rRer:   	1	3.12
hRer:   	1	3.45
MISer:  	2	6.25
EXTer:  	0	0.00
rLEXer: 	2	6.25
hLEXer: 	2	6.90

brINFer: 	2	6.25
bhINFer: 	2	6.90
brRer:   	1	3.12
bhRer:   	1	3.45
bMISer:  	2	6.25
bEXTer:  	0	0.00
brLEXer: 	2	6.25
bhLEXer: 	2	6.90
```
(to understand the output, refer to Popović 2011) and a number of files:
* `a.ref.tok` and `a.hyp.tok`: tokenized versions of the original files; 
* `a.ref.tok.base` and `a.hyp.tok.base`: files containing the base forms of words in `a.ref.tok` and `a.hyp.tok`
* `a.errors`: similar to the output above but for each line
* `a.html`: an HTML file (to be opened with a browser) in which color codes are used to describe errors (see Popović 2011 for details)


To catch the screen output in a file `a.report`, you can run the command like this:
```
python run-hjerson.py a es es_core_news_sm >a.report
```
### How to prepare your own files

(to be written)

## References

Popović, M. (2011). Hjerson: An open source tool for automatic error classification of machine translation output. The Prague Bulletin of Mathematical Linguistics, 96, 59-67.



